package hhn.qlqa027a.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Employee.class)
public abstract class Employee_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Employee, Date> birthday;
	public static volatile SingularAttribute<Employee, String> address;
	public static volatile SingularAttribute<Employee, Role> role;
	public static volatile SingularAttribute<Employee, String> fullName;
	public static volatile CollectionAttribute<Employee, Schedule> scheduleCollection;
	public static volatile SingularAttribute<Employee, String> password;
	public static volatile CollectionAttribute<Employee, Certificate> certificates;
	public static volatile SingularAttribute<Employee, String> phone;
	public static volatile SingularAttribute<Employee, String> identityNumber;
	public static volatile CollectionAttribute<Employee, Schedule> schedules;
	public static volatile CollectionAttribute<Employee, Bill> billCollection;
	public static volatile SingularAttribute<Employee, String> email;
	public static volatile SingularAttribute<Employee, Date> startDate;
	public static volatile SingularAttribute<Employee, Status> status;
	public static volatile CollectionAttribute<Employee, Employeeitems> employeeitemsCollection;

}

