package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Shift.class)
public abstract class Shift_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile CollectionAttribute<Shift, Schedule> scheduleCollection;
	public static volatile SingularAttribute<Shift, Short> maxManagers;
	public static volatile SingularAttribute<Shift, Short> minManagers;
	public static volatile SingularAttribute<Shift, Short> maxServicers;
	public static volatile SingularAttribute<Shift, Short> minBartenders;
	public static volatile SingularAttribute<Shift, String> name;
	public static volatile SingularAttribute<Shift, Short> maxBartenders;
	public static volatile SingularAttribute<Shift, Short> minServicers;
	public static volatile CollectionAttribute<Shift, Shiftnumbermember> shiftnumbermemberCollection;
	public static volatile SingularAttribute<Shift, Long> salary;

}

