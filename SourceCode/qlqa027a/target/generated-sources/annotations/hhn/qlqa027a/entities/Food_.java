package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Food.class)
public abstract class Food_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile CollectionAttribute<Food, Menufood> menufoodCollection;
	public static volatile SingularAttribute<Food, Status> statusId;
	public static volatile SingularAttribute<Food, Double> price;
	public static volatile SingularAttribute<Food, String> name;
	public static volatile CollectionAttribute<Food, Billdetail> billdetailCollection;
	public static volatile SingularAttribute<Food, Category> categoryId;

}

