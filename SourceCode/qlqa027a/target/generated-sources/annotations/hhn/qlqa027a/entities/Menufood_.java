package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Menufood.class)
public abstract class Menufood_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Menufood, Food> foodId;
	public static volatile SingularAttribute<Menufood, Menu> menuId;

}

