package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Rolepermissions.class)
public abstract class Rolepermissions_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Rolepermissions, Permission> permissionId;
	public static volatile SingularAttribute<Rolepermissions, Role> roleId;
	public static volatile SingularAttribute<Rolepermissions, Integer> isEnabled;

}

