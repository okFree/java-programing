package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Item.class)
public abstract class Item_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Item, String> name;
	public static volatile CollectionAttribute<Item, Employeeitems> employeeitemsCollection;

}

