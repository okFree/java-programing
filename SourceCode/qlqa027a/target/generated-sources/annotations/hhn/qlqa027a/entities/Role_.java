package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Role.class)
public abstract class Role_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Role, String> name;
	public static volatile CollectionAttribute<Role, Rolepermissions> rolepermissionsCollection;
	public static volatile CollectionAttribute<Role, Employee> employeeCollection;
	public static volatile CollectionAttribute<Role, Shiftnumbermember> shiftnumbermemberCollection;
	public static volatile SingularAttribute<Role, String> type;

}

