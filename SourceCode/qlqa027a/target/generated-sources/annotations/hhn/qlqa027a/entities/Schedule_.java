package hhn.qlqa027a.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Schedule.class)
public abstract class Schedule_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Schedule, Integer> minutesLate;
	public static volatile SingularAttribute<Schedule, Shift> shiftId;
	public static volatile SingularAttribute<Schedule, Date> workingDate;
	public static volatile SingularAttribute<Schedule, Status> statusId;
	public static volatile SingularAttribute<Schedule, Integer> isWorked;
	public static volatile SingularAttribute<Schedule, Employee> employeeId;

}

