package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Category.class)
public abstract class Category_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Category, Status> statusId;
	public static volatile SingularAttribute<Category, String> name;
	public static volatile CollectionAttribute<Category, Food> foodCollection;

}

