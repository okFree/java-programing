package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Employeeitems.class)
public abstract class Employeeitems_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Employeeitems, Item> itemId;
	public static volatile SingularAttribute<Employeeitems, Status> statusId;
	public static volatile SingularAttribute<Employeeitems, Employee> employeeId;

}

