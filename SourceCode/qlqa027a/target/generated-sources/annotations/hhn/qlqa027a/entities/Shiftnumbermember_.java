package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Shiftnumbermember.class)
public abstract class Shiftnumbermember_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Shiftnumbermember, Shift> shiftId;
	public static volatile SingularAttribute<Shiftnumbermember, Role> roleId;
	public static volatile SingularAttribute<Shiftnumbermember, Integer> numberEmployee;

}

