package hhn.qlqa027a.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Menu.class)
public abstract class Menu_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Menu, Date> createdAt;
	public static volatile CollectionAttribute<Menu, Menufood> menufoodCollection;
	public static volatile SingularAttribute<Menu, String> name;

}

