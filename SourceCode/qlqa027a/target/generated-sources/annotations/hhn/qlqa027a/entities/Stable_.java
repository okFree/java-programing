package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Stable.class)
public abstract class Stable_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Stable, Status> statusId;
	public static volatile SingularAttribute<Stable, String> name;
	public static volatile CollectionAttribute<Stable, Bill> billCollection;

}

