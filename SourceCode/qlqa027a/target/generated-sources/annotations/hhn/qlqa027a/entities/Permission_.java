package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Permission.class)
public abstract class Permission_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Permission, String> name;
	public static volatile CollectionAttribute<Permission, Rolepermissions> rolepermissionsCollection;

}

