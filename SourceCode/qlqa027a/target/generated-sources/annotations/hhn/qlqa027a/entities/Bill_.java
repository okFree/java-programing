package hhn.qlqa027a.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Bill.class)
public abstract class Bill_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Bill, Date> createdAt;
	public static volatile SingularAttribute<Bill, Integer> mergeBillId;
	public static volatile SingularAttribute<Bill, Double> amount;
	public static volatile SingularAttribute<Bill, Status> statusId;
	public static volatile SingularAttribute<Bill, Stable> tableId;
	public static volatile SingularAttribute<Bill, Integer> numberOfCustomer;
	public static volatile SingularAttribute<Bill, Double> customerPay;
	public static volatile SingularAttribute<Bill, Employee> employeeId;
	public static volatile CollectionAttribute<Bill, Billdetail> billdetailCollection;
	public static volatile SingularAttribute<Bill, Double> customerReturn;

}

