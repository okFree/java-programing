package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Billdetail.class)
public abstract class Billdetail_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile SingularAttribute<Billdetail, Status> statusId;
	public static volatile SingularAttribute<Billdetail, Bill> billId;
	public static volatile SingularAttribute<Billdetail, Food> foodId;
	public static volatile SingularAttribute<Billdetail, String> description;
	public static volatile SingularAttribute<Billdetail, Integer> quantum;

}

