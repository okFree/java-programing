package hhn.qlqa027a.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Status.class)
public abstract class Status_ extends hhn.qlqa027a.entities.BaseEntity_ {

	public static volatile CollectionAttribute<Status, Schedule> scheduleCollection;
	public static volatile CollectionAttribute<Status, Stable> stableCollection;
	public static volatile CollectionAttribute<Status, Category> categoryCollection;
	public static volatile SingularAttribute<Status, String> name;
	public static volatile CollectionAttribute<Status, Employee> employeeCollection;
	public static volatile CollectionAttribute<Status, Bill> billCollection;
	public static volatile CollectionAttribute<Status, Billdetail> billdetailCollection;
	public static volatile CollectionAttribute<Status, Food> foodCollection;
	public static volatile CollectionAttribute<Status, Employeeitems> employeeitemsCollection;

}

